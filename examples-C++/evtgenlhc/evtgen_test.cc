//
//  Sample test program for running EvtGen
//  
//  Created 1999-12-27 Ryd/Lange   
//  Modified by N. Davidson to demonstrate MC-TESTER with EvtGenLHC

#include "EvtGenBase/EvtPatches.hh"
#include "EvtGenBase/EvtPatches.hh"
#include <iostream>
#include "EvtGenBase/EvtParticleFactory.hh"
#include "EvtGenBase/EvtStdHep.hh"
#include "EvtGen/EvtGen.hh"
#include "EvtGenBase/EvtParticle.hh"
#include "EvtGenBase/EvtRandom.hh"
#include "EvtGenBase/EvtReport.hh"
#include <string>

#define NMXHEP 4000
extern struct hepevt {
int nevhep;		/* The event number */
int nhep;		/* The number of entries in this event */
int isthep[NMXHEP]; 	/* The Particle id */
int idhep[NMXHEP];      /* The particle id */
int jmohep[NMXHEP][2];    /* The position of the mother particle */
int jdahep[NMXHEP][2];    /* Position of the first daughter... */
double phep[NMXHEP][5];    /* 4-Momentum, mass */
double vhep[NMXHEP][4];    /* Vertex information */
} hepevt_;

//MC-TESTER header file
#include "Generate.h"

// Definition of the C structure representing Fortran common block HEPEVT
#if 0 /* 0 or 1 */
// Note: NMXHEP=nmxhep=4000 and double precision
#include "HEPEVTEvent.H"
extern "C" HepevtCommon hepevt_;
#else /* 0 or 1 */
// Note: default is NMXHEP=10000 and double precision
#define HEPMC3_HEPEVT_NMXHEP 4000
//#define HEPMC3_HEPEVT_PRECISION float
#include "HepMC3/HEPEVT_Wrapper.h"
extern "C" struct HEPEVT hepevt_;
#endif /* 0 or 1 */

using std::endl;
using std::cout;

//Define random number fcn used by Jetset
extern "C" {
  extern float rlu_();
  extern float begran_(int *);
}

float rlu_(){
  return EvtRandom::Flat();
}

float begran_(int *){
  return EvtRandom::Flat();
}

int main(int argc, char* argv[]){

  //Initialize MC-TESTER
  MC_Initialize();

  EvtStdHep evtstdhep;
  EvtParticle *parent;

  //Initialize the generator - read in the decay table and particle properties
  EvtGen myGenerator("./DECAY.DEC","./evt.pdl");

  static EvtId B_plus=EvtPDL::getId(std::string("B+"));

  int nEvents=10000;

  // Loop to create nEvents, starting from an Upsilon(4S)
  for(int i=0;i<nEvents;i++){
    if(i%1000==0)
      cout<<i<<" ("<<i*100.0/nEvents<<"%)"<<endl;

    // Set up the parent particle
    EvtVector4R pInit(EvtVector4R(EvtPDL::getMass(B_plus),0.0,0.0,0.0));
    parent=EvtParticleFactory::particleFactory(B_plus,pInit);

    // Generate the event
    myGenerator.generateDecay(parent);

    evtstdhep.init();

    // Write out the results
    parent->makeStdHep(evtstdhep);
    

    //Copy results to hepevt common block from stdhep
    //This part of the code should not be necessary for
    //EvtGen (non-LHC versions)??
    hepevt_.nevhep=i;
    hepevt_.nhep=evtstdhep.getNPart();    
    for(int pcle = 0; pcle < evtstdhep.getNPart(); pcle++){
      hepevt_.isthep[pcle]=evtstdhep.getIStat(pcle);
      hepevt_.idhep[pcle]=evtstdhep.getStdHepID(pcle);

      hepevt_.jmohep[pcle][0]=evtstdhep.getFirstMother(pcle)+1; //first pcle at 1 not 0.
      hepevt_.jmohep[pcle][1]=evtstdhep.getLastMother(pcle)+1;
      hepevt_.jdahep[pcle][0]=evtstdhep.getFirstDaughter(pcle)+1;
      hepevt_.jdahep[pcle][1]=evtstdhep.getLastDaughter(pcle)+1;

      /* 4-Momentum, mass */
      hepevt_.phep[pcle][0]=evtstdhep.getP4(pcle).get(1); //px
      hepevt_.phep[pcle][1]=evtstdhep.getP4(pcle).get(2); //py
      hepevt_.phep[pcle][2]=evtstdhep.getP4(pcle).get(3); //pz
      hepevt_.phep[pcle][3]=evtstdhep.getP4(pcle).get(0); //e
      hepevt_.phep[pcle][4]=evtstdhep.getP4(pcle).mass();

      /* Vertex information */
      hepevt_.vhep[pcle][0]=evtstdhep.getX4(pcle).get(1); //x
      hepevt_.vhep[pcle][1]=evtstdhep.getX4(pcle).get(2); //y
      hepevt_.vhep[pcle][2]=evtstdhep.getX4(pcle).get(3); //z
      hepevt_.vhep[pcle][3]=evtstdhep.getX4(pcle).get(0); //t
    }

    //Analyse the B+ decay
    MC_Analyze(521);

    //clean up
    parent->deleteTree();  
  }
  
  //Finalize MC-TESTER
  MC_Finalize();

  return 1;
}
