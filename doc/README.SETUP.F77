****) To find general description of MC-TESTER setup parameters, refer to
README.SETUP file.

A set of FORTRAN77 subroutines was provided to allow modification of some
MC-Tester's parameters. These functions are implemented in C++ and 
the following interface is provided:
---------------------

    SUBROUTINE MCSETUP( WHAT, VALUE)
    
    INTEGER WHAT
    INTEGER VALUE

Description and parameters:
    WHAT: specifies what kind of value needs to be set
	= 0 	Event record structure to be used:
	    VALUE=0	COMMON/HEPEVT/ in 4k-D format
	    VALUE=1	COMMON/LUJETS/ (i.e. Pythia 5.7)
	    VALUE=2	COMMON/PYJETS/ (i.e. Pythia 6)
	    VALUE=3 HerwigEVT - data from HEPEVT interpretted using specific
				HERWIG gramatics
	    VALUE=4	COMMON/MCTEVT/ - internal MC-TESTER's event record
	    
	=1	generation stage, VALUE=1 or 2 for first
	    and second generator respectively. Look at
	    tauola example - stage is introduced to two
	    version of code using preprocessor.
	    
	=2	PDG code of particle, which decays we analyze.
	
	
----------------------
    SUBROUTINE MCSETUPHBINS(VALUE)
    
    INTEGER VALUE
    
Description and parameters:
    Sets up default number of bins in histograms.
    	
----------------------
    SUBROUTINE MCSETUPHMIN(VALUE)
    
    DOUBLE PRECISION VALUE
    
Description and parameters:
    Sets up the value of minimum bin in histograms.
    Note that the parameter must be in DOUBLE PRECISION !
    	
----------------------
    SUBROUTINE MCSETUPHMAX(VALUE)
    
    DOUBLE PRECISION VALUE
    
Description and parameters:
    Sets up the value of maximum bin in histograms.
    Note that the parameter must be in DOUBLE PRECISION !
    	

----------------------
    SUBROUTINE MCSETUPHIST(NBODY,NHIST,NBINS,MINBIN,MAXBIN)
    
    INTEGER NBODY,NHIST,NBINS
    DOUBLE PRECISION MINBIN,MAXBIN
    
Description and parameters:
    Sets up the parameters for histograms of
    NHIST - body subsystems in NBODY - bodies decay channel.
    
    NBINS is number of bins
    MINBIN is minimum bin value
    MAXBIN is maximum bin value.
    
    Note that MINBIN and MAXBIN are DOUBLE PRECISION!

--------------------------------------------------------------------------------
Last modified by Piotr Golonka 26-October-2003.
