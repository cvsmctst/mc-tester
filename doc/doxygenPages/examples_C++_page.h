/*! \page examples_C Generation Step with C++ Monte-Carlo Generators

  \sa examples-C++ directory

  This page describes the contents of the examples-C++ subdirectory and gives an example of how
  MC-TESTER can be used on event records of the type HepMC::GenEvent.
  Currently the directory contains the libraries needed by MC-TESTER for HepMC events and an example 
  of the generation step for PYTHIA 8.1. We would like to extend the C++ examples to Herwig++ in the future.

  <hr>

  \section libs_for_HepMC Libraries for Processing HepMC Events

  Many C++ Monte-Carlo generators and particle physics experiments use the common event record format 
<A HREF="http://lcgapp.cern.ch/project/simu/HepMC/">HepMC</A> as a container for events. The 
libraries located in the directory /examples-C++/HepMClibs provide an interface between HepMC events 
and events interpretable by MC-TESTER ("HEPEvent"). Please see the documentation for the classes
in this library:
- HepMCEvent which translates between HepMC::GenEvent and HEPEvent
- HepMCParticle which translates between HepMC::GenParticle and HEPParticle. 

The library headers must be included when running MC-TESTER on an event in HepMC format.

  <hr>

  \section running Running MC-TESTER in a C++ Monte-Carlo Generators Main Loop

  MC-TESTER can be run on a HepMC Event in the following way:

  The following MC-TESTER header files must be included:
  \verbatim
  #include "Generate.h"
  #include "HepMCEvent.H"  \endverbatim

  In the main function, initialize MC-TESTER:
  \verbatim
  MC_Initialize(); \endverbatim  

  MC-TESTER processes events using either of the functions
  \verbatim
  MC_Analyze(HEPEvent * event, double weight=1.0);  or
  MC_Analyze(int particle, double weight=1.0, HEPEvent * event=0); \endverbatim  

  A HepMCEvent is derived from both HEPEvent and HepMC::GenEvent.
  This allows a way of passing the HepMC::GenEvent information to the tester, 
  without forcing the interface to take the specific event record format HepMC::GenEvent. 
  An example of how to do this is:

  \verbatim
  HepMCEvent * temp_event = new HepMCEvent(*HepMCEvt);
  MC_Analyze(temp_event);

  delete temp_event; \endverbatim 

  Finally, at the end of the main function MC-TESTER is finalized and the ROOT 
  output file is created by including the statement:
  \verbatim  
  MC_Finalize(); \endverbatim 

  <hr>

  \section pythia_8_example PYTHIA 8.1 Example

An example for the PYTHIA 8.1 event generator is given in the directory
examples-C++/pythia/. Please look at the pythia_test.cc file to see how the above MC-TESTER 
functions can be utilized.

This example generates 10,000 e+e- -> Z0 -> taus events. The decay of the taus is analyzed by 
MC-TESTER. The output, mc-tester.root, contains the results of the processed events and
includes a number of histograms which can be compared to similar output
from other Monte-Carlo generators. Configuration of the tool is done via 
the examples-C++/pythia/SETUP.C file.

To run the example, the <A HREF="http://home.thep.lu.se/~torbjorn/Pythia.html">PYTHIA 8.1</A> 
and <A HREF="http://lcgapp.cern.ch/project/simu/HepMC/">HepMC 2</A> packages must be installed. 
PYTHIA 8.1 must be compiles with HepMC 2 and the pythia library hepmcinterface must exists.
The location to the base of the /include and /lib directories for these packages should be
set through the variables PYTHIA_INSTALL_LOCATION and HEPMC_INSTALL_LOCATION. You will also need to set the
environmental variable PYTHIA8DATA to the directory containing PYTHIA xml documents 
(generally it should be "$(PYTHIA_INSTALL_LOCATION)/xmldoc").

After installing and making MC-TESTER you can run the example in the examples-C++/pythia
subdirectory with the commands:
  
\verbatim 
make
./pythiatest.exe
make move1 (or make move2) \endverbatim

The final step moves the output file (mc-tester.root) to the directory /analyse/prod1
ready for the analysis step. A second output should be produced using a different
Monte-Carlo Generator and moved to /analyse/prod2. Follow the instructions in 
"\ref documentation" to see examples of how to produce another file that can be
used in a comparison.

<hr>

 \section example_c_results Example Results for PYTHIA 8.1 (C++) vs PYTHIA 6.4.14 (Fortan)

 The booklet produced by MC-TESTER for 1,000,000 events using the PYTHIA C++ and
PYTHIA Fortran examples can be found 
<a href="http://mc-tester.web.cern.ch/MC-TESTER/mc-tester_results/tester_6.4vs8.1.pdf">here</a>.

*/
