/*! \page configuration_scripts New installation procedure and bugfixes

  @section configuration New installation procedure

  The 1.24 version introduces configuration scripts generated with autoconfig. For new installation insctructions refer to 'Installation' section on main doxygen webpage.
  Similar script have been provided for examples-C++/pythia example.

 @section bugfixes Changes and bugfixes

 - Fixed bug with ANALYZE.C crashing in some cases when compared data files have several not matched channels.
 - Fixed bug with UserTreeAnalysis scripts crashing during program execution if using MC4Vector inside the script.
 - All warnings with '-ansi -pedantic' compiler flags have been removed.

*/
