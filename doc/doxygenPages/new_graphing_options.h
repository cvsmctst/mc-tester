/*! \page new_graphing_options New Graphical Improvements 

MC-TESTER v1.22 introduces three new options that enchance histogram readability.

@section logY Logarithmic Scale option

This options enables the use of logarithmic scale for all histograms created at analysis step, therefore making histograms from both generators represented in logarithmic scale, along with the right-hand Y axis. This option does not affect the SDP calculation nor changes the SDP histogram representation which still remains in it's linear scale.

@section scaleX Scaling of the mass axis

Adding this option at the generation step allows all invariant masses to be scalled to invariant mass
of all daughters combined, which in fact scales the X axis to the range (0,1). When using this option consider setting default maximum bin value to 1.1

@section massPower Invariant mass power

When used at generation step, this option modifies the calculation of the invariant mass, changing it's power from the default value of 1 to a higher one. This also modifies the indication displayed on histograms to show, that the power of the mass has been changed.

<hr>

For further informations regarding the use of these options, check the updated MC-TESTER documentation, or look at the SETUP.C file in /analyze folder for an example of use.

*/