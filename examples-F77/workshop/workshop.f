      PROGRAM TESTERDEM
C     *****************

C===============================
C initialization of MC-TESTER  =
C===============================
C we'll use HEPEVT: 
      CALL MCSETUP(0,0)
C decay of WORKSHOP Particle (PDG=100)
      CALL MCSETUP(2,100) 
C  generation stage (generator id): 1
      CALL MCSETUP(1,1)
C Histograms set-up:
C 120 bins per histogram     
      CALL MCSETUPHBINS(120)
C default minimum bin value is 0.0D0
      CALL MCSETUPHMIN(0.0D0)
C default maximum bin value is 90.0D0 (1.5 times cms energy for w-shop.
      CALL MCSETUPHMAX(90D0)

C execute initialization of  MC-Tester
      CALL MCTEST(-1)
C======================================
C end of initialization of MC-TESTER  =
C======================================

C Here come calls to your programs, just running in this case.
      CALL TESTES

C finalization of MC-TESTER actions:
      CALL MCTEST(1)
      END


      SUBROUTINE TESTES
C     *****************

      NEVTES=1
 
      DO 300 IEV=1,NEVTES

         CALL PSEUDOEVENT               !  pseudo_generator
         IF (IEV.eq.1) call MCTEST(20)  ! print the HEPEVT
         CALL MCTEST(0)                 ! MC-TESTER is called
         CALL MCTEST(21)	        ! print modified record used by tester
  300 CONTINUE

      RETURN
      END
      
      SUBROUTINE PSEUDOEVENT
C     ======================
C     This PSEUDOEVENT can need fill of HEPEVT with up to 8 final
C     state fermions. The TOHEPI routine should work with any
C     of these cases, as well as with your programs 
      DIMENSION  XPB1(4),XPB2(4),AQF1(4),AQF2(4)
      DIMENSION   AQF3(4),AQF4(4) ,AQF5(4),AQF6(4)  ,AQF7(4),AQF8(4) 
      DOUBLE PRECISION XPB1, XPB2, AQF1, AQF2, AQF3, AQF4
      DOUBLE PRECISION AQF5, AQF6, AQF7, AQF8
C      IN FINAL STATE THERE ARE N PARTICLES .... 
        N=6

!      Some of them may be dummy

C HERE COME THEIR IDENTIFIERS, ACCORDINGLY TO PARTICLE DATA GROUP CONVENTIONS
C JUST IN CASE WE FILL VALUES FOR 8 FINAL STATESBUT ONLY FIRST N WILL BE USED.

C beams ...
        KFB1= 11
        KFB2=-11

C final states ...

        IDF1= 13
        IDF2=-13
        IDF3= 12
        IDF4=-12
        IDF5= 14
        IDF6=-14
        IDF7= 15
        IDF8=-15

C MOMENTA OF THE BEAMS:
        XPB1(1)= 0.
        XPB2(1)= 0.
        XPB1(2)= 0.
        XPB2(2)= 0.
        XPB1(3)= 5.*N
        XPB2(3)=-5.*N 
        XPB1(4)= 5.*N
        XPB2(4)= 5.*N 

C MOMENTA OF THE FIRST PAIR OF FERMIONS:
        AQF1(1)= 0.
        AQF2(1)= 0.
        AQF1(2)= 0.
        AQF2(2)= 0.
        AQF1(3)= 10.
        AQF2(3)=-10.
        AQF1(4)= 10.
        AQF2(4)= 10.

C MOMENTA OF THE SECOND PAIR OF FERMIONS:
        AQF3(1)= 10.
        AQF4(1)=-10.
        AQF3(2)= 0.
        AQF4(2)= 0.
        AQF3(3)= 0.
        AQF4(3)= 0.
        AQF3(4)= 10.
        AQF4(4)= 10.

C MOMENTA OF THE THIRD PAIR OF FERMIONS:
        AQF5(1)= 0.
        AQF6(1)= 0.
        AQF5(2)= 10.
        AQF6(2)=-10.
        AQF5(3)= 0.
        AQF6(3)= 0.
        AQF5(4)= 10.
        AQF6(4)= 10.

C MOMENTA OF THE FOURTH PAIR OF FERMIONS:
        AQF7(1)= 5.
        AQF8(1)=-5.
        AQF7(2)= 5.
        AQF8(2)=-5.
        AQF7(3)= 0.
        AQF8(3)= 0.
        AQF7(4)= 10.
        AQF8(4)= 10.
       
      CALL LCTOHEP(N,KFB1,KFB2,
     $ IDF1,IDF2,IDF3,IDF4,IDF5,IDF6,IDF7,IDF8,
     $ XPB1,XPB2,AQF1,AQF2,AQF3,AQF4,AQF5,AQF6,AQF7,AQF8)

      END

