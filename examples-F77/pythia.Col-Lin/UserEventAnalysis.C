class UserEventAnalysis: public TObject {

private:
    HEPEVTEvent *save_event;

public:
    UserEventAnalysis();
    virtual ~UserEventAnalysis();
    
    virtual void SaveOriginalEvent(HEPEvent *e);
    virtual void RestoreOriginalEvent(HEPEvent *e);
    virtual HEPEvent* ModifyEvent(HEPEvent *e);

};

UserEventAnalysis::UserEventAnalysis()
{
    save_event=HEPEVTEvent::CreateEvent(4000);
}

UserEventAnalysis::~UserEventAnalysis()
{
    if (save_event) delete save_event;
}

void UserEventAnalysis::SaveOriginalEvent(HEPEvent *e)
{
    printf("SAVE ORIGINAL EVENT ...\n");
}

void UserEventAnalysis::RestoreOriginalEvent(HEPEvent *e)
{
    printf("RESTORE ORIGINAL EVENT ...\n");
}

HEPEvent* ModifyEvent(HEPEvent *e)
{
    printf("MODIFY EVENT\n");

    return e;
}