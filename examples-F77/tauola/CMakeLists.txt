if (NOT TARGET MC-TESTER::HEPEvent)
  find_package(MC-TESTER  REQUIRED)
endif() 
if (NOT TARGET ROOT::Core)
  find_package(ROOT REQUIRED)
endif() 

file (READ gen1/demo-standalone/taumain.f TAUDEMO1_SRC)
string(REGEX REPLACE "PROGRAM" "SUBROUTINE" TAUFACE1_SRC "${TAUDEMO1_SRC}")
file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/gen1/tauface-tester.f "${TAUFACE1_SRC}")

file (READ gen2/demo-standalone/taumain.f TAUDEMO2_SRC)
string(REGEX REPLACE "PROGRAM" "SUBROUTINE" TAUFACE2_SRC "${TAUDEMO2_SRC}")
file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/gen2/tauface-tester.f "${TAUFACE2_SRC}")


add_library(glib1 STATIC gen1/formf.f gen1/tauola.f )
add_library(glib2 STATIC gen2/formf.f gen2/tauola.f gen2/curr_cleo.f gen2/f3pi.f gen2/pkorb.f)

add_executable(tautest1 tautest.F ${CMAKE_CURRENT_BINARY_DIR}/gen1/tauface-tester.f)
target_link_libraries(tautest1 PRIVATE  MC-TESTER::HEPEvent MC-TESTER::MCTester ROOT::Core glib1)
target_compile_definitions(tautest1 PRIVATE  -DSTAGE1)

add_executable(tautest2 tautest.F ${CMAKE_CURRENT_BINARY_DIR}/gen2/tauface-tester.f)
target_link_libraries(tautest2 PRIVATE  MC-TESTER::HEPEvent MC-TESTER::MCTester ROOT::Core glib2)
target_compile_definitions(tautest2 PRIVATE  -DSTAGE2)



if (MC-TESTER_ENABLE_TESTING)
   file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/gen1)
   file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/gen2)
#   add_test(NAME tautest1 COMMAND tautest1  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/gen1)
#   add_test(NAME tautest2 COMMAND tautest2 WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/gen2)
endif()
