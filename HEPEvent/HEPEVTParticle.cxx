/*
   HEPEVTParticle class implementation

   AUTHOR:      Piotr Golonka
   LAST UPDATE: 2000-01-17
   COPYRIGHT:   (C) Faculty of Nuclear Physics & Techniques, UMM Cracow.
*/

#include "HEPEVTParticle.H"
#include "HEPEVTEvent.H"

#ifdef _USE_ROOT_
ClassImp(HEPEVTParticle)
#endif


HEPEVTParticle::HEPEVTParticle()
{
 id=1;
 event=&HEPEVT;
}

void HEPEVTParticle::ls (const char* option )
{
   HEPParticle::ls(option);
}


const HEPEVTParticle HEPEVTParticle::operator=(HEPParticle &p)   
{
  if (this == &p)
    return *this;

  // SetId(p.GetId());

  SetPDGId(p.GetPDGId());
  SetStatus(p.GetStatus());
  SetMother(p.GetMother());
  SetMother2(p.GetMother2());
  SetFirstDaughter(p.GetFirstDaughter());
  SetLastDaughter(p.GetLastDaughter());
  SetE(p.GetE());
  SetPx(p.GetPx());
  SetPy(p.GetPy());
  SetPz(p.GetPz());
  SetM(p.GetM());
  SetVx(p.GetVx());
  SetVy(p.GetVy());
  SetVz(p.GetVz());
  SetTau(p.GetTau());

  return *this;

}


inline HEPEvent* HEPEVTParticle::GetEvent()
{ 
  return (HEPEvent*) event; 
}

inline int const HEPEVTParticle::GetId()
{ 
   return id;
 }

inline int const HEPEVTParticle::GetMother()
{ 
   return event->GetJMOHEP_(1,id);
}

inline int const HEPEVTParticle::GetMother2()
{ 
   return event->GetJMOHEP_(2,id); 
}

inline int const HEPEVTParticle::GetFirstDaughter()
{ 
   return event->GetJDAHEP_(1,id);
}

inline int const HEPEVTParticle::GetLastDaughter()
{ 
   return event->GetJDAHEP_(2,id);
}

inline double const HEPEVTParticle::GetE ()
{ 
   return event->GetPHEP_(4,id);
}

inline double const HEPEVTParticle::GetPx()
{ 
   return event->GetPHEP_(1,id);
}

inline double const HEPEVTParticle::GetPy()
{
   return event->GetPHEP_(2,id);
}

inline double const HEPEVTParticle::GetPz()
{ 
return event->GetPHEP_(3,id);
}

inline double const HEPEVTParticle::GetM()
{
   return event->GetPHEP_(5,id);
}

inline int const HEPEVTParticle::GetPDGId()
{ 
   return event->GetIDHEP_(id);
}

inline int const HEPEVTParticle::GetStatus()
{
   return event->GetISTHEP_(id);
}

inline double const HEPEVTParticle::GetVx()
{ 
   return event->GetVHEP_(1,id);
}

inline double const HEPEVTParticle::GetVy()
{
   return event->GetVHEP_(2,id);
}

inline double const HEPEVTParticle::GetVz()
{
   return event->GetVHEP_(3,id);
}

inline double const HEPEVTParticle::GetTau()
{ 
   return event->GetVHEP_(4,id);
}

inline void HEPEVTParticle::SetEvent( HEPEvent  *event ) 
{
  this->event=(HEPEVTEvent*)event;
}

inline void HEPEVTParticle::SetId( int id       )
{
   this->id=id;
}     

inline void HEPEVTParticle::SetMother( int mother )
{ 
  event->SetJMOHEP_(1,id,mother);
}

inline void HEPEVTParticle::SetMother2( int mother ) 
{
   event->SetJMOHEP_(2,id,mother);
}

inline void HEPEVTParticle::SetFirstDaughter( int daughter )
{
   event->SetJDAHEP_(1,id,daughter);
}

inline void HEPEVTParticle::SetLastDaughter( int daughter )
{
    event->SetJDAHEP_(2,id,daughter); 
}

inline void HEPEVTParticle::SetE( double E )
{
   event->SetPHEP_(4,id,E ); 
}

inline void HEPEVTParticle::SetPx( double px )
{
   event->SetPHEP_(1,id,px); 
}

inline void HEPEVTParticle::SetPy( double py )
{ 
   event->SetPHEP_(2,id,py); 
}

inline void HEPEVTParticle::SetPz( double pz )
{
   event->SetPHEP_(3,id,pz); 
} 

inline void HEPEVTParticle::SetM( double m )
{
   event->SetPHEP_(5,id,m ); 
}

inline void HEPEVTParticle::SetPDGId( int pdg )
{
   event->SetIDHEP_(id,pdg); 
}

inline void HEPEVTParticle::SetStatus( int st )
{ 
   event->SetISTHEP_(id,st); 
}

inline void HEPEVTParticle::SetVx( double vx )
{
   event->SetVHEP_(1,id,vx); 
}

inline void HEPEVTParticle::SetVy( double vy )
{
   event->SetVHEP_(2,id,vy); 
}

inline void HEPEVTParticle::SetVz( double vz )
{
   event->SetVHEP_(3,id,vz); 
}

inline void HEPEVTParticle::SetTau( double tau )
{
   event->SetVHEP_(4,id,tau); 
}






#ifdef _USE_ROOT_
void HEPEVTParticle::Streamer(TBuffer &)
{
  // streamer class for ROOT compatibility
}
#endif   
