/*
   GenerationDescription class implementation
*/
#include "GenerationDescription.H"


ClassImp(GenerationDescription)


GenerationDescription::GenerationDescription() 
{

    //////////////////////////////////////////////////////////
    // Setup histograms bounds for 1- 2- ... n-body decays. //
    //////////////////////////////////////////////////////////


    // default numbers of bins
    int maxbins=128;

    // default maximal bin value (min==0.0)
    double defaultmaxbin=4.0;


    // fill default values...
    for (int nbody=0;nbody<MAX_DECAY_MULTIPLICITY;nbody++) {
    for (int nhist=0;nhist<MAX_DECAY_MULTIPLICITY;nhist++) {

	nbins  [nbody][nhist]=maxbins;
	bin_min[nbody][nhist]=0.0;
	bin_max[nbody][nhist]=defaultmaxbin;
    }
    }

}

GenerationDescription::GenerationDescription(const GenerationDescription &gd) 
{
    decay_particle=gd.decay_particle;
    order_matters=gd.order_matters;

    for (int nbody=0;nbody<MAX_DECAY_MULTIPLICITY;nbody++) {
    for (int nhist=0;nhist<MAX_DECAY_MULTIPLICITY;nhist++) {

	nbins  [nbody][nhist]=gd.nbins[nbody][nhist];
	bin_min[nbody][nhist]=gd.bin_min[nbody][nhist];
	bin_max[nbody][nhist]=gd.bin_max[nbody][nhist];
    }
    }

    strncpy(gen_desc_1,gd.gen_desc_1,128);
    strncpy(gen_desc_2,gd.gen_desc_2,128);
    strncpy(gen_desc_3,gd.gen_desc_3,128);
    strncpy(gen_path,gd.gen_path,128);
}

GenerationDescription::GenerationDescription(const Setup &s) 
{
    decay_particle=s.decay_particle;
    order_matters=s.order_matters;

    for (int nbody=0;nbody<MAX_DECAY_MULTIPLICITY;nbody++) {
    for (int nhist=0;nhist<MAX_DECAY_MULTIPLICITY;nhist++) {

	nbins  [nbody][nhist]=s.nbins[nbody][nhist];
	bin_min[nbody][nhist]=s.bin_min[nbody][nhist];
	bin_max[nbody][nhist]=s.bin_max[nbody][nhist];
    }
    }
    
}


