/**
 * @brief MC-TESTER FORTRAN interface declaration
 *
 * This file contains routines that can be executed from FORTRAN code
 * Descriptions of these routines are presented in FORTRAN style
 */
#ifndef __MCTESTER_F77_H_
#define __MCTESTER_F77_H_


extern "C" void mctest_(int &mode); // F77 binding function/
    // mode=-1 ->MC_Initialize()
    // mode=0 MC_Analyze()
    // mode=1 MC_Finalize()
    // mode=20 print HEPEVT event

// version of MC-TESTER filling for weighted events...
extern "C" void mctestw_(int &mode, double &weight);


extern "C" void mcsetup_(int &what, int &value);
    // what = 0 : Event record standard:
		// value = 0 : HEPEVT
		// value = 1 : LUJETS
		// value = 2 : PYJETS
    // what = 1 : stage; (value=1,2 for generation);
    // what = 2 : particle to be analysed, PDG as value

extern "C" void mcsetuphbins_(int &value); // set default number of bins
extern "C" void mcsetuphmin_(double &value);// set default minimum bin
extern "C" void mcsetuphmax_(double &value);// set default maximum bin
extern "C" void mcsetuphist_(int &nbody, int &nhist, int &nbins, double &minbin, double &maxbin);// setup certain histogram



/****************************************************************************
      SUBROUTINE LCTOHEP(N,KFB1,KFB2,
                 $ IDF1,IDF2,IDF3,IDF4,IDF5,IDF6,IDF7,IDF8,
                 $ XPB1,XPB2,AQF1,AQF2,AQF3,AQF4,AQF5,AQF6,AQF7,AQF8)
 
C     ===================================================
C     THIS IS THE ROUTINE YOU WILL USE:
C     N             - number of particles in final state
C     KFB1,KFB2     - beam identifiers
C     IDF1,IDF2,... - identifiers of FS particles
C     XPB1,XPB2,... - beam four momenta
C     AQF1,AQF2,... - final state partcls four momenta
C     IDFn,AQFn     - for n.GT.N are dummy
C     ===================================================

  DIMENSION  XPB1(4),XPB2(4),AQF1(4),AQF2(4)
  DIMENSION  AQF3(4),AQF4(4),AQF5(4),AQF6(4),AQF7(4),AQF8(4)
*******************************************************************************/
			  
			  
extern "C" void lctohep_(int &n,
    int &kfb1, int &kfb2,
    int &idf1, int &idf2, int &idf3, int &idf4,
    int &idf5, int &idf6, int &idf7, int &idf8,
    double xpb1[4], double xpb2[4],
    double aqf1[4], double aqf2[4], double aqf3[4], double aqf4[4],
    double aqf5[4], double aqf6[4], double aqf7[4], double aqf8[4] );



extern "C" void mcthbk_( int &hist_id, int &nbins, double &bin_min, double &bin_max);
/*
        SUBROUTINE MCTHBK(HIST_ID, NBINS, BIN_MIN, BIN_MAX)

        INTEGER HIST_ID
        INTEGER NBINS
        REAL*8 BIN_MIN
        REAL*8 BIN_MAX

 C---------------------------------------------------------------------
 C Creates user's histogram in the MC-TESTER infrastructure.
 C Histogram may then be filled using CALL MCTHFIL(HIST_ID,VALUE,W)
 C
 C HIST_ID - unique integer ID. In the root file histogram will
 C           be called h_XXXX wher XXXX is specified HIST_ID
 C
 C NBINS   - number of bins in histogram
 C BIN_MIN - lower edge of histogram
 C BIN_MAX - upper edge of histogram
 C---------------------------------------------------------------------
*/


extern "C" void mcthfil_( int &hist_id, double &value, double &weight);
/*
        SUBROUTINE MCTHFIL(HIST_ID, VALUE, WEIGHT)

        INTEGER HIST_ID
        REAL*8 VALUE
        REAL*8 WEIGHT
*/



			

// for ROOT:

#ifdef __CINT__
#pragma link C++ function mctest_;
#pragma link C++ function mctestw_;
#pragma link C++ function mcsetup_;
#pragma link C++ function mcsetuphbins_;
#pragma link C++ function mcsetuphmin_;
#pragma link C++ function mcsetuphmax_;
#pragma link C++ function mcsetuphist_;
#pragma link C++ function mcthbk_;
#pragma link C++ function mcthfil_;
#endif



#endif
